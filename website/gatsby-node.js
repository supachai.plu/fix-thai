/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

const path = require('path');
const fs = require('fs');

/*
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

exports.onCreateWebpackConfig = ({
    stage,
    rules,
    loaders,
    plugins,
    actions,
  }) => {
    actions.setWebpackConfig({
        output: {
            path: path.resolve(process.cwd(), '../public')
        }, 
        plugins: [
            new CleanWebpackPlugin({
                
            })
        ],
    })
  }
*/

exports.onPostBuild = function() {
/*
    if( !fs.lstatSync(path.join(__dirname, '../public')).isDirectory() ){
        fs.mkdirSync(path.join(__dirname, '../public'));
    }

    if( fs.lstatSync(path.join(__dirname, '../public')).isDirectory() ){
        fs.renameSync(path.join('../public'), path.join(__dirname, '../public-del'));
    }

    if( fs.lstatSync(path.join(__dirname, '../public-del')).isDirectory() ){
    //fs.rmdirSync(path.join(__dirname, '../public-del'));
        rmDir(path.join(__dirname, '../public-del'));
    }
*/
    /*
    if( fs.existsSync(path.join(__dirname, '../public')) ){
        fs.rmdirSync(path.join(__dirname, '../public'));
        //rmDir(path.join(__dirname, '../public'));
    }

    fs.renameSync(path.join(__dirname, 'public'), path.join(__dirname, '../public'));
    */
};


const rmDir = function(dir, rmSelf) {
    var files;
    rmSelf = (rmSelf === undefined) ? true : rmSelf;
    dir = dir + "/";
    try { files = fs.readdirSync(dir); } catch (e) { console.log("!Oops, directory not exist."); return; }
    if (files.length > 0) {
        files.forEach(function(x, i) {
            if (fs.statSync(dir + x).isDirectory()) {
                rmDir(dir + x);
            } else {
                fs.unlinkSync(dir + x);
            }
        });
    }
    if (rmSelf) {
        // check if user want to delete the directory ir just the files in this directory
        fs.rmdirSync(dir);
    }
}