import React, { Component } from "react"
import styled, {css} from "styled-components";

const Container = styled.div`
    display: flex;
    width: 100%;
    margin: 10px 0;
    align-items: baseline;
    letter-spacing: 2px;

    ${props => props.marginBottom && css`
        margin-bottom : ${props.marginBottom} !important;
    `}

    ${props => props.marginTop && css`
        margin-top : ${props.marginTop} !important;
    `}
    
    ${props => props.marginLeft && css`
        margin-left : ${props.marginLeft} !important;
    `}
`

const Text = styled.div`
    font-size: 2.0rem;
    font-weight: 400;
    letter-spacing: 0px;
    
    @media screen and (max-width: 415px) {
      font-size: 1.3rem;
      font-weight: 600;
    }
`

const DashLine = styled.div`
    flex: 1 1;
    border-bottom: 2px solid black;
    margin-left: 8px;
    margin-bottom: 4px;

    ${props => props.line && css`
        border-bottom : ${props.line} solid !important;
    `}
`

const Title = ({ lineId="", title= "", marginBottom="", customTitle="", line="", marginTop="", marginLeft="", style={} }) => (
    <Container marginBottom={marginBottom} marginTop={marginTop} marginLeft={marginLeft} style={style}>
        {
            customTitle ?
            customTitle
            :
            <Text style={style}>{title}</Text>
        }
        <DashLine id={lineId} line={line}/>
    </Container>
)

export default Title
