/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React, { useEffect, useRef } from "react"
import PropTypes from "prop-types"
//import { useStaticQuery, graphql } from "gatsby"
import styled from "styled-components";
import Header from "./header"
import Flag from '../../images/Flag_of_Thailand.svg'
import "./layout.css"

import $ from "jquery";
//import Raphael from '../../assets/js/map/raphel.min.js';
import Raphael from '../../../assets/js/jsmaps/jsmaps-libs.js'
//import imges from '../../assets/images/flag-thailand-1.png'

import '../../../assets/js/jsmaps/jsmaps.css';

if( typeof window !== `undefined` ){
  window.Raphael= Raphael;
  
  window.jQuery = $;
  window.$ = $;
  global.jQuery = $;

  require('../../../assets/js/jsmaps/jsmaps.js');
  //require('../../assets/js/jsmaps/jsmaps-libs.js');
  require('../../../assets/js/jsmaps/thailand.js');
}

const Footer = styled.div`
  background-color: black;
  color: white;
  font-weight: 600;
  padding: 8px;
  text-align: center;
`

const Title = styled.div`
  margin: 0px;
  color: black;
  text-shadow: black 0px 0px;
  text-align: center;
  background: white;
  font-size: 1.5rem;
  opacity: 0.8;
`

const BG = styled.div`
  background: url(${Flag});
  background-size: contain;
  position: absolute;
  top: 0;
  z-index: -1;
  width: 100%;
  height: 100%;
  opacity: 0.9;
`

const Layout = ({ children }) => {

  const header = useRef(undefined);
  const body = useRef(undefined);
  const footer = useRef(undefined);

  useEffect( () => {
    (async function anyNameFunction() {
      console.log( header.current.clientHeight )
      console.log( body.current.clientHeight )
      console.log( footer.current.clientHeight )
      
      body.current.style.minHeight = `calc( 100vh - ( ${header.current.clientHeight}px + ${footer.current.clientHeight}px + 1.45rem ) )`
      /*
      */
      //console.log( body.current.style.height )
      //await getDeviceInfo();
    })();
  }, [])

  /*
  const getDeviceInfo = async () => {
    const md = new MobileDetect(window.navigator.userAgent);
    console.log( window.navigator.userAgent )
    console.log( md )
    
    console.log( await getDeviceIp() );
    console.log( md.mobile() );          // 'Sony'
    console.log( md.phone() );           // 'Sony'
    console.log( md.tablet() );          // null
    console.log( 'userAgent', md.userAgent() );       // 'Safari'
    console.log( md.os() );              // 'AndroidOS'
    console.log( md.is('iPhone') );      // false
    console.log( md.is('bot') );         // false
    console.log( md.version('Webkit') );         // 534.3
    console.log( md.versionStr('Build') );       // '4.1.A.0.562'
    console.log( md.match('playstation|xbox') ); // false

    //var md = new MobileDetect(userAgent, parseInt(maxPhoneWidth, 10)),
    var rules = MobileDetect._impl.mobileDetectRules;
    console.log( 'rules', rules );

    Object.keys(rules.props).forEach(function (propKey) {
      var version;
      version = md.versionStr(propKey);
      if (version) {
        console.log({key: 'versionStr("' + propKey + '")', val: '"' + version + '"'});
      }
      version = md.version(propKey);
      if (version) {
        console.log({key: 'version("' + propKey + '")', val: version});
      }
    });


    const deviceInfo = {
      ip: await getDeviceIp(),
      os: md.os(),
      browser: md.userAgent,
    }
  }
  */

  return (
    <>
      {<Header refs={header} siteTitle={'Fix Thai (ส่งเสียงเล็กๆของคุณ เพื่อเปลี่ยนประเทศไทย)'} />}
      <div
        ref={body}
        style={{
          margin: `0 auto`,
          maxWidth: 960,
          padding: `0 1.0875rem 1.45rem`
        }}
      >
        <main>{children}</main>
      </div>
      <Footer ref={footer}>
        <br/>
        © {new Date().getFullYear()}, Develop By {` `} Galleria. All Rights Reserved.
        <br/>
        <br/>
      </Footer>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
