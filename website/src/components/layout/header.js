import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import Flag from '../../images/Flag_of_Thailand.svg'
import styled from 'styled-components'

const Title = styled.div`
  margin: 0px;
  color: black;
  text-shadow: black 0px 0px;
  text-align: center;
  background: white;
  font-size: 1.5rem;
  opacity: 0.8;
`

const BG = styled.div`
  background: url(${Flag});
  background-size: contain;
  position: absolute;
  top: 0;
  z-index: -1;
  width: 100%;
  height: 100%;
  opacity: 0.9;
`

const Header = ({ refs, siteTitle }) => (
  <div
    style={{
      marginBottom: `1.45rem`,
      height: '40vw', 
      maxHeight: '120px',
      position: 'relative',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    }}
    ref={refs}
  >
    <div
      style={{
        margin: `0 auto`,
        maxWidth: 960,
        padding: `1.45rem 1.0875rem`,
      }}
    >
      <Title>
        {siteTitle}
      </Title>
    </div>
    <BG/>
  </div>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
