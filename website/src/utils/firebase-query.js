// must be listed before other Firebase SDKs
//import * as firebase from "firebase/app";

import uuidv4 from 'uuid/v4'
import moment from 'moment'

// Add the Firebase services that you want to use
import "firebase/auth";
import "firebase/firestore";
import "firebase/database";

var firebase = require('firebase/app');

var firebaseConfig = {
    apiKey: " AIzaSyCHOVCBAO1cfpPDwUOyCZbOBG_hPMAqqCk ",
    authDomain: "https://fix-thai.firebaseapp.com/",
    databaseURL: "https://fix-thai.firebaseio.com",
    projectId: "fix-thai",
    storageBucket: "fix-thai.appspot.com",
    appId: "1:1082353805827:web:dffdee9aab6d913d5a7c36",
};
  
  // Initialize Firebase
var defaultProject = firebase.default.initializeApp(firebaseConfig);

//var defaultDatabase = firebase.database();
//var defaultStorage = defaultProject.storage();
var defaultFirestore = defaultProject.firestore();


export const insertRequest = async ( { topic, comment, location } ) => {

  let data = {
    _id: `${moment().format('YYYYMMDD')}-${uuidv4()}`,
    topic: '',
    comment: '',
    location: {
      lat: '',
      lng: '',
      address: ''
    },
    images: [],
    contact: {
      name: '',
      phone: '',
      email: ''
    },
    created_date: new Date(),
    updated_date: new Date()
  }

  try{
    const result = defaultFirestore.collection("request").doc(data._id).set(data)
    console.log("Document written with ID: ", result.id);
  }catch(e){
    console.error("Error adding document: ", e);
  }

}

export const getAllRequest = () => {
  try {
    let jobsRef = await defaultFirestore.collection('request').get();
    let jobList = await !jobsRef.empty ? Promise.all( jobsRef.docs.map( async doc => await doc.data() )) : [];

    return jobList
  } catch (e) {
      console.error("Error retriving document: ", e);
  } finally{
      //LoadingState(false)
  }
}

export const getRequestById = ( id ) => {
  try{
    let result = undefined;

    let jobsRef = defaultFirestore.collection('request');
    let jobs = await jobsRef.where('_id', '==', id).get()

    console.log('jobs', jobs )
    if( !jobs.empty ){
      result = jobs.docs[0].data()
    }

    return result
  } catch (e) {
    console.error("Error adding document: ", e);
  } finally{
    
  }

}

