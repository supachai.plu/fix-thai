import Pica from 'pica'
import moment from 'moment'
import axios from 'axios'
import uuidv4 from 'uuid/v4'

const url = 'https://us-central1-fix-thai.cloudfunctions.net/app'

function getCanvasBlob(canvas) {
    return new Promise(function(resolve, reject) {
      canvas.toBlob(function(blob) {
        resolve(blob)
      })
    })
}

export const getDeviceIp = async () => {
    const url = `https://api.ipify.org?format=json`

    const result = await axios.get(url)
    console.log( result )
    return result.data.ip
}
  
export const insertRequestFormService = async ( jobObj ) => {
    const pica = Pica();

    let result = undefined;

    let images = []
    
    for( let i=0 ; i < jobObj.images.length ; i++ ){
        try{
            console.log( jobObj.images[i] )

            var img = new Image();
            img.src = jobObj.images[i].url;

            let imgResize = document.createElement("canvas");
            imgResize.height = img.height / 2;
            imgResize.width = img.width / 2;

            console.log( img.height, imgResize.height )
            console.log( img.width, imgResize.width )
            
        
            let resizeImg = await pica.resize( img , imgResize)
            console.log( resizeImg.toDataURL() )

            // images.push( jobObj.images[i] )
            images.push( await getCanvasBlob(resizeImg) )
            
        }catch(e){
            console.error( 'error converting img', e )
        }
    }

    let data = {
        //_id: `${moment().format('YYYYMMDD')}-${uuidv4()}`,
        ...jobObj,
        //img_url: img_url,
        //created_date: new Date(),
        //updated_date: new Date()
    }
    
    try {
        //result = await defaultFirestore.collection("job").doc(data._id).set(data)
        result = await axios.post( url +'/request-fix', data)
        console.log("Document written with ID: ", result.id);
    } catch (e) {
        console.error("Error adding document: ", e);
    }

    return result

}

export const getRequestFormList = async () => {
    try {
        //result = await defaultFirestore.collection("job").doc(data._id).set(data)
        let result = await axios.get( url +'/request-fix')
        console.log("Document written with ID: ", result);
        return result
    } catch (e) {
        console.error("Error adding document: ", e);
    }
}

export const getRequestFormById = async (id) => {
    try {
        //result = await defaultFirestore.collection("job").doc(data._id).set(data)
        let result = await axios.get( url +'/request-fix/'+id)
        console.log("Document written with ID: ", result);
        return result
    } catch (e) {
        console.error("Error adding document: ", e);
    }
}