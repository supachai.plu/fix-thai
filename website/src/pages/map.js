import React, { useEffect } from "react"

import Layout from "../components/layout/layout"
import SEO from "../components/layout/seo"
import jQuery from "jquery";

const $ = jQuery


const MapPage = () => {

    useEffect( () => {
        $('#thailand-map').JSMaps({
            map: 'thailand'
        });
    }, [])


    return (
        <Layout>
          <div className="jsmaps-wrapper" id="thailand-map"></div>
        </Layout>
    )
}

export default MapPage
