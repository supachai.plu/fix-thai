import React, { useState, useRef, useEffect } from "react"
import { Link, navigateTo } from "gatsby"

import Layout from "../components/layout/layout"

import { ImagePicker, WingBlank, SegmentedControl, Toast, WhiteSpace, InputItem, TextareaItem } from 'antd-mobile';
import { Form, Button } from 'antd';

import Title from "../components/title";
import GoogleMapReact from 'google-map-react';
import { insertRequestFormService, getRequestFormById } from "../utils/api-service";
import styled from "styled-components";
import queryString from 'query-string';

import axios from 'axios';
import moment from 'moment';
import 'moment/locale/th';
import "firebase/auth";
var firebase = require('firebase/app');

const HRLine = styled.div`
    margin: 8px;
    line-height: 1em;
    position: relative;
    outline: 0;
    border: 0;
    color: black;
    text-align: center;
    height: 1.5em;
    opacity: .5;
    &:before {
    content: '';
    // use the linear-gradient for the fading effect
    // use a solid background color for a solid bar
    background: linear-gradient(to right, transparent, #818078, transparent);
    position: absolute;
    left: 0;
    top: 50%;
    width: 100%;
    height: 1px;
    }
    &:after {
    content: attr(data-content);
    position: relative;
    display: inline-block;
    color: black;

    padding: 0 .5em;
    line-height: 1.5em;
    // this is really the only tricky part, you need to specify the background color of the container element...
    color: #818078;
    background-color: #fcfcfa;
    }
`

const WhiteSpaceCustom = styled( props => <WhiteSpace {...props} />)`
    height: 12px;
`

const Marker = (props) => {
    const { color, name, id } = props;
    return (
      <div className="marker"
        style={{ backgroundColor: color, cursor: 'pointer'}}
        title={name}
      />
    );
};

const FormPage = ({ form }) => {

    const { getFieldDecorator, getFieldValue, setFieldsValue } = form

    let [ location, setLocation ] = useState([])
    let [ map , setMap ] = useState(undefined)
    let [ maps, setMaps ] = useState(undefined)
    let [ mapDragEvent, setMapDragEvent ] = useState(undefined)
    let [ mapCenterEvent, setMapCenterEvent ] = useState(undefined)
    
    let [ request_form, setRequestForm ] = useState(undefined)
    let [ data, setData ] = useState(undefined)

    const props = {
        multiple: true,
        maximize: 10,
        minimize: 2,

        center: {
            lat: 13.736717,
            lng: 100.523186
        },
        zoom: 11
    }

    const geo_options = {
        enableHighAccuracy: true, 
        maximumAge        : 30000, 
        timeout           : 27000
    }

    useEffect( () => {
        (async function anyNameFunction() {
            moment.locale('th');

            await checkQueryParams();

            var firebaseConfig = {
                apiKey: " AIzaSyCHOVCBAO1cfpPDwUOyCZbOBG_hPMAqqCk",
                authDomain: "https://fix-thai.firebaseapp.com/",
                projectId: "fix-thai",
                appId: "1:1082353805827:web:dffdee9aab6d913d5a7c36",
            };

            if (!firebase.apps.length) {
                firebase.default.initializeApp(firebaseConfig);
                await signInAnonymouse()
            }

        })();
    }, [] )

    const checkQueryParams = async () => {
        if( typeof window !== 'undefined' ){
            const params = queryString.parse(window.location.search);
            if( params.request_form ){
                setRequestForm(params.request_form)
                const resp = await getRequestFormById(params.request_form)
                setData( resp.data )
            }
        }
    }
        
    const handleApiLoaded = (map, maps) => {
        setMap(map)
        setMaps(maps)
    }

    const customLocation = () => {
        const mapEvent = map.addListener('drag', function() {
            const center = map.getCenter();
            setLocation( [center.lat(), center.lng()] )
        });
        setMapDragEvent( mapEvent )

        const mapEvent1 = map.addListener('center_changed', function() {
            const center = map.getCenter();
            setLocation( [center.lat(), center.lng()] )
        });
        setMapCenterEvent(mapEvent1)
        
        const center = map.getCenter();
        setLocation( [center.lat(), center.lng()] )
    }

    const endCustomLocation = () => {
        maps.event.removeListener(mapDragEvent);
        maps.event.removeListener(mapCenterEvent);
        setMapDragEvent( undefined )
    }

    const onSelectImages = (files, type, index) => {
        if( files.length <= props.maximize ){
            const imgs = getFieldValue('images')
            setFieldsValue({ images: files })
        }else{
            Toast.fail( `โปรดเลือกรูปไม่เกิน ${props.maximize} รูป`  )   
        }
    }

    const getLocation = () => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, geo_error, geo_options);
        } else {
            Toast.fail( `Geolocation is not supported by this browser.`  )   
        }
    }

    const showPosition = (position) => {
        setLocation( [position.coords.latitude, position.coords.longitude ] )
        map.setCenter({ lat:position.coords.latitude, lng:position.coords.longitude });
        map.setZoom( 16 )
    }

    const geo_error = () => {
        alert("Sorry, no position available.");
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        form.validateFields( async (err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
            try{
                const result = await insertRequestFormService( values )
                if( result.data.error ){
                    Toast.fail( result.data.error, 15);
                }else{
                    Toast.success( 'Insert Success', 5);
                    navigateTo('/')
                }
            }catch(e){
                alert( e.toString() )
                //navigateTo('/404')
            }
            //navigateTo('/bill-board')
          }
        });
    }

    const signInAnonymouse = async () => {
        try{
            await firebase.auth().signInAnonymously()
            firebase.auth().onAuthStateChanged(function(user) {
                if (user) {
                  // User is signed in.
                  var isAnonymous = user.isAnonymous;
                  var uid = user.uid;
                  // ...
                  console.log( uid, isAnonymous )
                  //setUId( uid )
                  firebase.auth().currentUser.getIdToken().then(function(token) {
                    axios.defaults.headers.common['x-firebase-idtoken'] = token;
                  })
                } else {
                  // User is signed out.
                  // ...
                }
                // ...
              });
        } catch(error){
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log( 'error', error )
        }
    }

    return (
        <Layout>
            <Form onSubmit={handleSubmit} className="form">

                {
                    request_form && data &&
                    <>
                        <Title title={"หัวข้อเรื่อง"}></Title>
                        <div> {data.topic} </div>

                        <Title title={"รูปภาพ"}></Title>
                        <div> 
                            { data.img_url.map( img_url => <img src={ img_url } style={{ width: '100%' }} /> ) }
                        </div>

                        <Title title={"คอมเม้น"}></Title>
                        <div> {data.comment} </div>

                        {/*
                        <Title title={"ตำแหน่ง"}></Title>
                        <div style={{ height: '35vh', width: '100%', border: '1px solid #a2a2a252',marginBottom: '16px' }}>
                            <GoogleMapReact
                                bootstrapURLKeys={{ key: 'AIzaSyCRt99sLAj2kM4-GltXLYHH2vEo7uZitiU' }}
                                defaultCenter={ props.center }
                                defaultZoom={ props.zoom }
                                onGoogleApiLoaded={({ map, maps }) => handleApiLoaded(map, maps)}
                                //ref={refMap}
                            >
                                <Marker
                                    lat={location[0]}
                                    lng={location[1]}
                                    name={"ตำแหน่งของคุณ"}
                                    color={"red"}
                                />
                            </GoogleMapReact>
                        </div>
                        */}

                        <Title title={"ข้อมูลผู้ส่ง"}></Title>
                        <div>{data.name}</div>
                        <div>{data.tel}</div>
                        <div>{data.email}</div>

                        <Title title={"วันที่แจ้งปัญหา"}></Title>
                        <div>{ moment(data.created_date._seconds).format('DD MMM YYYY hh:mm')}</div>
                    </>
                }

                {
                    !request_form &&
                    <>
                        <Title title={"หัวข้อเรื่อง"}></Title>
                        { getFieldDecorator('topic') ( <InputItem placeholder="หัวข้อ" /> ) }
                        

                        <Title title={"แนบรูปภาพ"}></Title>
                        { getFieldDecorator('images', { initialValue: [] }) (
                            <ImagePicker
                                files={ getFieldValue('images') }
                                onChange={onSelectImages}
                                onImageClick={(index, fs) => console.log(index, fs)}
                                selectable={getFieldValue('images').length < props.maximize}
                                multiple={props.multiple}
                                accept="image/*;" 
                                capture="camera"
                            />
                        )}
        
        
                        <Title title={"คอมเม้น"}></Title>
                        { getFieldDecorator('comment') (
                            <TextareaItem
                                //title="标题"
                                placeholder="อยากให้ปรับปรุงอะไรพิมพ์มาเลย"
                                //data-seed="logId"
                                //ref={el => this.autoFocusInst = el}
                                rows={8}
                                count={250}
                                autoHeight
                            />
                        )}
        
                        <Title title={"ระบุตำแหน่ง"}></Title>
                        <Button type="primary" onClick={getLocation} style={{ backgroundColor: 'orange', borderColor: 'orange', width: '100%' }} disabled={ mapDragEvent } > กดเพื่อระบุตำแหน่งของคุณ </Button>
        
                        <HRLine data-content="หรือ" />
                        {
                            !mapDragEvent ?
                            <Button type="primary" onClick={customLocation} style={{ backgroundColor: 'red', borderColor: 'red', width: '100%' }} > เลือกตำแหน่งเอง </Button>
                            :
                            <Button type="primary" onClick={endCustomLocation} style={{ backgroundColor: 'red', borderColor: 'red', width: '100%' }} > ยืนยันตำแหน่ง </Button>
                        }
                        <WhiteSpace />
                        <WhiteSpace />
                        <div style={{ height: '35vh', width: '100%', border: '1px solid #a2a2a252',marginBottom: '16px' }}>
                            <GoogleMapReact
                                bootstrapURLKeys={{ key: 'AIzaSyCRt99sLAj2kM4-GltXLYHH2vEo7uZitiU' }}
                                defaultCenter={ props.center }
                                defaultZoom={ props.zoom }
                                onGoogleApiLoaded={({ map, maps }) => handleApiLoaded(map, maps)}
                                //ref={refMap}
                            >
                                <Marker
                                    lat={location[0]}
                                    lng={location[1]}
                                    name={"ตำแหน่งของคุณ"}
                                    color={"red"}
                                />
                            </GoogleMapReact>
                        </div>
        
                        <Title title={"ข้อมูลผู้ส่ง (ไม่จำเป็นต้องกรอก)"}></Title>
        
                        { getFieldDecorator('name') ( <InputItem placeholder="ชื่อ/นามแฝง" > ชื่อ/นามแฝง </InputItem> )}
                        <WhiteSpaceCustom />
                        
                        { getFieldDecorator('tel') ( <InputItem placeholder="0819990000" > เบอร์โทรศัพท์ </InputItem> )}
                        <WhiteSpaceCustom />
        
                        { getFieldDecorator('email') ( <InputItem placeholder="example@example.com" > อีเมลล์ </InputItem> )}
        
                        <WhiteSpace />
                        <WhiteSpace />
                        <WhiteSpace />
        
                        <Button type="primary" htmlType="submit" style={{ backgroundColor: '#4BB543', borderColor: '#4BB543', width: '100%' }} >ส่งข้อมูล</Button>
                        <WhiteSpace />
                    </>
                }
                
            </Form>
        </Layout>
    )
}


const WrapperFormPage = Form.create({ 
    name: 'request_form',
    mapPropsToFields(props){
        return {

        } 
    }
})(FormPage)

export default WrapperFormPage
