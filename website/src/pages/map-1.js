
//5f24013ea7bc20bb5f345a8a464f55f2 

import React, { useEffect } from "react"

import Layout from "../components/layout/layout"
import SEO from "../components/layout/seo"
import jQuery from "jquery";
import { longdo, map, LongdoMap } from "../components/LongdoMap";

const $ = jQuery


const Map1Page = () => {

    useEffect( () => {
        //map.Layers.setBase(longdo.Layers.GRAY);
    }, [])

    const initMap = () => {
        map.Layers.setBase(longdo.Layers.GRAY);

        let marker = new longdo.Marker({ lon: 100.56, lat: 13.74 });
        map.Overlays.add(marker);

        //map.Overlays.remove(marker);
        //map.Overlays.clear();
    }

    const mapKey = '5f24013ea7bc20bb5f345a8a464f55f2'

    return (
        <Layout>
            <div style={{ height: '100vh' }}>
                <LongdoMap id="longdo-map" mapKey={mapKey} callback={initMap} />
            </div>
        </Layout>
    )
}

export default Map1Page
