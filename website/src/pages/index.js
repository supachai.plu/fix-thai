import React, { useEffect, useState } from "react"
import { Link, navigateTo } from "gatsby"

import Layout from "../components/layout/layout"
import SEO from "../components/layout/seo"
import { getRequestFormList } from "../utils/api-service"
import { Card, Row, Col, Button } from "antd"
import Meta from "antd/lib/card/Meta"
import { navigate } from "@reach/router"

const IndexPage = () => {

  let [ data, setData ] = useState([])

  useEffect( () => {
    (async function anyNameFunction() {
      await loadRequestForm();

    })();
  }, [])

  const loadRequestForm = async () => {
    const result = await getRequestFormList()
    setData( result.data )
  }

  return(
    <Layout>
      <Button onClick={ () => navigate('/request-form') }> Create Request </Button>
      <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
        {
          data.map( (d) => {
            return (
              <Col className="gutter-row" span={6}>
                <Card
                  hoverable
                  style={{ width: 240 }}
                  cover={<img src={ d.img_url[0] || 'http://via.placeholder.com/240x160' } />}
                  onClick={ () => navigateTo(`/request-form/?request_form=${d._id}`) }
                >
                  <Meta title={d.topic} description={d.comment} />
                  {new Date(d.created_date._seconds).toString()}
                </Card>
              </Col>
            )
          } )
        }
      </Row>
    </Layout>
  )
}

export default IndexPage
