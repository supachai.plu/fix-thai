module.exports.firebaseConfig = {
    apiKey: " AIzaSyCHOVCBAO1cfpPDwUOyCZbOBG_hPMAqqCk",
    authDomain: "https://fix-thai.firebaseapp.com/",
    databaseURL: "https://fix-thai.firebaseio.com",
    projectId: "fix-thai",
    storageBucket: "fix-thai.appspot.com",
    appId: "1:1082353805827:web:dffdee9aab6d913d5a7c36",
};

const admin = require('firebase-admin');

admin.initializeApp({ 
    credential: admin.credential.applicationDefault(),
    //databaseURL: "https://fix-thai.firebaseio.com",
    storageBucket: "fix-thai.appspot.com",
})
  
const defaultFirestore = admin.firestore()
const defaultStorage = admin.storage();

module.exports.defaultFirestore = defaultFirestore;
module.exports.defaultStorage = defaultStorage;