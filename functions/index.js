const functions = require('firebase-functions');
const express = require('express');
const multer = require('multer');
const port = 3000;
const { getBlockIpByIp, insertBlockIp, removeBlockIp } = require('./firebase-blockip')
const { insertRequestForm, getRequestById, getRequests } = require('./firebase-request-form')
const { insertStatistic } = require('./firebase-statistic')

const cors = require('cors')
const upload = multer({ inMemory: true })
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const app = express();
const whiteListCountry = [ 'TH' ]
const whiteListURL = [ 'https://fix-thai.firebaseapp.com/' ]

const corsOptions = {
    origin: function (origin, callback) {
        if (whiteListURL.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
}

app.use(cors( { origin: true } ));


app.listen( port, function(){
    console.log("Listening to Port "+ port)
} )

app.get('/ping', (req, res)=> {
    res.send( "Warhp" )
})

app.post('/request-fix', upload.fields([ { name: 'images', maxCount: 10 } ]), async (req, res) => {
    try{
        const ipAddress = req.headers['x-appengine-user-ip']
        const country = req.headers['x-appengine-country']
        const city = req.headers['x-appengine-city']

        console.log( req.headers["x-forwarded-for"], req.headers['x-appengine-user-ip'] )
        console.log( req.headers['x-appengine-country'] )
        console.log( req.headers['x-appengine-city'] )
        console.log( req )
        console.log( req.body )

        if( whiteListCountry.includes( country ) && !(await getBlockIpByIp(ipAddress)) ){
            const result = await insertRequestForm( req.body )
            await insertStatistic(country, city)
            //await insertBlockIp(ipAddress)
            //res.send( _geolocation(req, res) )
            res.send( result )
        }else{
            await insertStatistic(country, city, true)
            res.send( {
                error: 'Your IP Couldnot Post Now, Please wait 15 minunute.'
            } )
        }
    }catch(e){
        res.status(400).send('SomeThing Wrong')
    }
});

app.get('/request-fix', async (req, res) => {
    /*
    defaultFirestore.collection("request-fix").get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            console.log(`${doc.id} => ${doc.data()}`);
        });
    });
    */
   res.send( await getRequests() )
})

app.get('/request-fix/:id', async (req, res) => {
    /*
    defaultFirestore.collection("request-fix").get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            console.log(`${doc.id} => ${doc.data()}`);
        });
    });
    */
   res.send( await getRequestById( req.params.id ) )
})

const _geolocation = (req, res) => {
    // res.header('Cache-Control','no-cache');
    const data = {
        country: req.headers["x-appengine-country"],
        region: req.headers["x-appengine-region"],
        city: req.headers["x-appengine-city"],
        cityLatLong: req.headers["x-appengine-citylatlong"],
        userIP: req.headers["x-appengine-user-ip"]
    }

    res.json(data)
};


exports.clearBlockIp = functions
  .pubsub.schedule("* /15 * * * *")
  .timeZone('Asia/Bangkok')
  .onRun( async (context) => {
    console.info("This will be run every 15 minutes!");
    await removeBlockIp()
});


exports.app = functions.https.onRequest(app);