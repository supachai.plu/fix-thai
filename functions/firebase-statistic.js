
const { defaultFirestore } = require('./config');
const moment = require('moment');

const collection = 'statistic'

module.exports.insertStatistic = async (country, city, isSpam) => {
    try {
        console.log( 'insertStatistic ')
        let result = undefined;

        let jobsRef = defaultFirestore.collection( collection );
        let jobs = await jobsRef.where('country', '==', country).where('city', '==', city).get()

        const date_time = moment().format('DD-MM-YYYY')

        if ( jobs.empty ) {

            const data = {
                country,
                city,
                //count: 1,
                //spam_count: 1,
                created_date: new Date(),
                updated_date: new Date()
            }

            data[date_time] = { [ isSpam ? 'spam_count' : 'count' ]: 1 }
    
            result = defaultFirestore.collection( collection ).doc( `${country}-${city}` ).set(data);

        }else{

            let dataDoc = jobs.docs[0].data()
            let dataRef = jobs.docs[0].ref

            let time = {}

            if ( dataDoc[ `${date_time}` ] ) {
                time = { ...dataDoc[ `${date_time}` ] }
                if( isSpam ){
                    time['spam_count'] = time['spam_count'] ? time['spam_count'] += 1 : 1
                }else{
                    time['count'] = time['count'] ? time['count'] += 1 : 1
                }
            } else {
                time[ isSpam ? 'spam_count' : 'count' ] = 1//isSpam ? (dataDoc.spam_count ? dataDoc.spam_count += 1 : 1) : (dataDoc.count += 1);
            }

            let updated_date = new Date()

            result = dataRef.update({ [`${date_time}`]: {...time}, updated_date })

        }

        return result
    } catch (e) {
        console.error("Error adding document: ", e);
    } finally{
        
    }
}