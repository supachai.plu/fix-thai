const { defaultStorage } = require('./config')
const moment = require('moment')
require("firebase/storage")
const uuidv4 = require('uuid/v4')

module.exports.uploadImageFile = async (file) => {
    try{
        var imgName = '/abc/' + (new moment().format('YYYYMMDDHHmmss')) + '.png'
        console.log( 'defaultStorage', defaultStorage )
        var bucket = defaultStorage.bucket();
        const fileBucket = bucket.file(imgName);

        base64EncodedImageString = file.url.replace(/^data:image\/\w+;base64,/, ''),
        console.log( 'base64EncodedImageString', base64EncodedImageString )
        imageBuffer = new Buffer(base64EncodedImageString, 'base64');

        const uploadResult = await fileBucket.save(imageBuffer, {
            metadata: { contentType: 'image/jpeg', 
            metadata: {
                firebaseStorageDownloadTokens: uuidv4()
            }, },
            gzip: true,
            public: true,
            validation: 'md5'
        })
        console.log( 'uploadResult', uploadResult )
        /*, function(error) {
        
            if (error) {
                return res.serverError('Unable to upload the image.');
            }
        
            return res.ok('Uploaded');
        });
        */
        
        /*
        var storageRef = defaultStorage.ref();//defaultStorage.bucket()
        var imagesRef = storageRef.child('request-form/'+ imgName);

        var blob = file

        var metadata = {
            contentType: 'image/jpeg',
        };

        const resultImg = await imagesRef.upload(blob, metadata);
        const imgUrl = await resultImg.ref.getDownloadURL();
        console.log( '====================> ', imgUrl, resultImg )
        */
        return `https://storage.googleapis.com/${bucket.name}${imgName}`

    } catch (e) {
        console.error('Error uploadImage File', e)
        throw e
    }
}