const { defaultFirestore } = require('./config')

//const defaultProject = firebase.default.initializeApp(firebaseConfig);
//console.log( 'defaultProject', defaultProject )
//const defaultStorage = defaultProject.storage();
//const defaultFirestore = defaultProject.firestore();

module.exports.getBlockIpByIp = async (ip) => {
    try {
        console.log( 'getBlockIpById ', ip )
        let result = undefined;

        let jobsRef = defaultFirestore.collection('block-ip');
        let jobs = await jobsRef.where('ip', '==', ip).get()

        console.log('jobs', jobs )
        console.log('jobs', jobs.empty )
        if( !jobs.empty ){
            result = jobs.docs[0].data()
        }
        return result
    } catch (e) {
        console.error("Error adding document: ", e);
    } finally{
        
    }
}

module.exports.insertBlockIp = async (ip, country) => {
    try {
        console.log( 'insertBlockIpBy ', ip )

        const data = {
            ip,
            created_date: new Date()
        }
        
        const result = defaultFirestore.collection('block-ip').doc(ip).set(data);
        //let jobs = await jobsRef.where('ip', '==', ip).get()
        return result
    } catch (e) {
        console.error("Error adding document: ", e);
    } finally{
        
    }
}

module.exports.removeBlockIp = async () => {
    try {
        console.log( 'removeBlockIp ' )

        const jobsRef = defaultFirestore.collection('block-ip')
        //let jobs = await jobsRef.where('ip', '==', ip).get()

        var MS_PER_MINUTE = 60000;
        var MINS_15 = MS_PER_MINUTE * 15
        var myStartDate = new Date( new Date() - MINS_15);
        var myEndDate = new Date()

        let jobs = await jobsRef.where( 'created_date', '<=', myStartDate ).get()
        console.log('jobs', jobs )
        console.log('jobs', jobs.empty )
        if( !jobs.empty ){
            jobs.docs.map( job => console.log( job.data() ) )
        }

        return ''
    } catch (e) {
        console.error("Error adding document: ", e);
    } finally{
        
    }
}