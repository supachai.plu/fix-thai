const { defaultFirestore } = require('./config')
const { uploadImageFile } = require('./firebase-upload-image');
const moment = require('moment')
const uuidv4 = require('uuid/v4')

module.exports.getRequestById = async (id) => {
    try {
        console.log( 'getRequestById ', id )
        let result = undefined;

        let jobsRef = defaultFirestore.collection('job');
        let jobs = await jobsRef.where('_id', '==', id).get()

        console.log('request-form', jobs )
        console.log('request-form', jobs.empty )
        if( !jobs.empty ){
            result = jobs.docs[0].data()
        }
        return result
    } catch (e) {
        console.error("Error adding document: ", e);
    } finally{
        
    }
}

module.exports.getRequests = async () => {
    try {
        console.log( 'getRequest ' )
        let result = undefined;

        let jobsRef = defaultFirestore.collection('job');
        let jobs = await jobsRef.get()

        console.log('request-form', jobs )
        console.log('request-form', jobs.empty )
        
        result = await !jobs.empty ? Promise.all( jobs.docs.map( async doc => await doc.data() )) : [];
        return result
    } catch (e) {
        console.error("Error adding document: ", e);
    } finally{
        
    }
}

module.exports.insertRequestForm = async ( obj ) => {
    try {
        console.log( 'insertRequestForm ', obj )

        let img_url = []
        for( let i=0 ; i < obj.images.length ; i++ ){
            try{
                img_url.push( await uploadImageFile( obj.images[i] ) )
            }catch(e){
                console.error( 'error upload img', e )
            }
        }
    
        delete obj.images;

        let data = {
            _id: `${moment().format('YYYYMMDD')}-${uuidv4()}`,
            ...obj,
            img_url: img_url,
            created_date: new Date(),
            updated_date: new Date()
        }

        let result = undefined;

        try {
            result = await defaultFirestore.collection("job").doc(data._id).set(data)
            console.log("Document written with ID: ", result.id);
        } catch (e) {
            console.error("Error adding document: ", e);
        }
        
        return result
    } catch (e) {
        console.error("Error adding document: ", e);
    } finally{
        
    }
}
